DROP database if exists bonhomme;
CREATE DATABASE bonhomme;

USE bonhomme;

CREATE TABLE jambes (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    burl VARCHAR(255)
);

GRANT ALL PRIVILEGES ON bonhomme.* TO 'ovsep'@'localhost';

